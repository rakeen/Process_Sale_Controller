System.register(['angular2/platform/browser', './components/processSaleController'], function(exports_1) {
    var browser_1, processSaleController_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (processSaleController_1_1) {
                processSaleController_1 = processSaleController_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(processSaleController_1.App);
        }
    }
});
//# sourceMappingURL=boot.js.map