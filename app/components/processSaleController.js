System.register(['angular2/core', 'angular2/http', 'rxjs/add/operator/map', './saleFactory', './sale'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, saleFactory_1, sale_1;
    var App;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {},
            function (saleFactory_1_1) {
                saleFactory_1 = saleFactory_1_1;
            },
            function (sale_1_1) {
                sale_1 = sale_1_1;
            }],
        execute: function() {
            App = (function () {
                function App(sale, http) {
                    var _this = this;
                    // setting the config
                    http.get('/app/config.json').map(function (response) { return response.json(); })
                        .subscribe(function (data) { return _this.res = data; }, function (err) { return console.error('There was an error while fetching the config.json file: ' + err); }, function () {
                        saleFactory_1.saleFactory.setConfig(_this.res.config);
                        console.log('successfully fetched config.json!', _this.res.config);
                    });
                    this.sale = sale;
                }
                App.prototype.makeNewSale = function () {
                    this.sale = new sale_1.Sale();
                    console.log('new sale!');
                };
                App.prototype.addItem = function () {
                    this.sale.addSaleLineItem(this.itemId, this.quantity);
                    // make the input fields empty again
                    this.itemId = null;
                    this.quantity = null;
                };
                App.prototype.getSale = function () {
                    return this.sale;
                };
                App = __decorate([
                    core_1.Component({
                        selector: 'app',
                        templateUrl: './app/components/processSaleController.html',
                        styleUrls: [],
                        providers: [sale_1.Sale, http_1.HTTP_BINDINGS]
                    }), 
                    __metadata('design:paramtypes', [sale_1.Sale, http_1.Http])
                ], App);
                return App;
            })();
            exports_1("App", App);
        }
    }
});
//# sourceMappingURL=processSaleController.js.map