System.register(['./BD/BDVATCalculator'], function(exports_1) {
    var BDVATCalculator_1;
    var BDVATAdapter;
    return {
        setters:[
            function (BDVATCalculator_1_1) {
                BDVATCalculator_1 = BDVATCalculator_1_1;
            }],
        execute: function() {
            BDVATAdapter = (function () {
                function BDVATAdapter() {
                    this.bdv = new BDVATCalculator_1.BDVATCalculator();
                }
                BDVATAdapter.prototype.getVATAmount = function (saleTotal) {
                    return parseInt(this.bdv.calculateVATAmount(saleTotal));
                };
                return BDVATAdapter;
            })();
            exports_1("BDVATAdapter", BDVATAdapter);
        }
    }
});
//# sourceMappingURL=BDVATAdapter.js.map