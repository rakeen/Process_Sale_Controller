export class BDVATCalculator{
	constructor() { }

	calculateVATAmount(saleTotal: number) {
		return saleTotal * 0.035;
	}
}