System.register([], function(exports_1) {
    var BDVATCalculator;
    return {
        setters:[],
        execute: function() {
            BDVATCalculator = (function () {
                function BDVATCalculator() {
                }
                BDVATCalculator.prototype.calculateVATAmount = function (saleTotal) {
                    return saleTotal * 0.035;
                };
                return BDVATCalculator;
            })();
            exports_1("BDVATCalculator", BDVATCalculator);
        }
    }
});
//# sourceMappingURL=BDVATCalculator.js.map