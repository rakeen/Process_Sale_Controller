System.register([], function(exports_1) {
    var productSpecification;
    return {
        setters:[],
        execute: function() {
            productSpecification = (function () {
                function productSpecification() {
                }
                productSpecification.prototype.getPrice = function () {
                    return this.price;
                };
                return productSpecification;
            })();
            exports_1("productSpecification", productSpecification);
        }
    }
});
//# sourceMappingURL=productSpecification.js.map