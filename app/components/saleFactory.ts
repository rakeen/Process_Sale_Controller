import {productSpecification} from './productSpecification';
import {IVATCalculator} from './ivatcalculator';
import {MyVATCalculator} from './MyVATCalculator';
import {BDVATAdapter} from './BDVATAdapter';



export class saleFactory{
	saleFactorySingleton: saleFactory;
	psList: Array<productSpecification>;
	vatCalculator: IVATCalculator;
	config: string;

	constructor(c:string){
		this.config = c;

		this.psList = new Array();
		


		// hard coded product specification
		var item = new productSpecification();

		item.id = '001A';
		item.name = 'macbook air';
		item.price = 1200;
		this.psList.push(item);

		item = new productSpecification();
		item.id = '001B';
		item.name = 'macbook';
		item.price = 950;
		this.psList.push(item);

		item = new productSpecification();
		item.id = '100';
		item.name = '100 dollar bill!';
		item.price = 100;
		this.psList.push(item);
	}

	getProductSpecification(id:string){

		return this.psList.filter(function(item) {
			return item.id === id;
		})[0];
	}


	getVatCalculator(){
		console.log(this.config);
		if(typeof this.vatCalculator==='undefined'){
			if(this.config==='MyVATCalculator')
				this.vatCalculator = new MyVATCalculator();
			else 
				this.vatCalculator = new BDVATAdapter();
		}
		return this.vatCalculator;
	}
	static getInstance(){
		if(typeof this.saleFactorySingleton==='undefined'){
			this.saleFactorySingleton = new saleFactory();
		}
		return this.saleFactorySingleton;
	}
	static setConfig(c:string){
		x=this.getInstance();
		x.config = c;
	}
}