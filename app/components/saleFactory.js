System.register(['./productSpecification', './MyVATCalculator', './BDVATAdapter'], function(exports_1) {
    var productSpecification_1, MyVATCalculator_1, BDVATAdapter_1;
    var saleFactory;
    return {
        setters:[
            function (productSpecification_1_1) {
                productSpecification_1 = productSpecification_1_1;
            },
            function (MyVATCalculator_1_1) {
                MyVATCalculator_1 = MyVATCalculator_1_1;
            },
            function (BDVATAdapter_1_1) {
                BDVATAdapter_1 = BDVATAdapter_1_1;
            }],
        execute: function() {
            saleFactory = (function () {
                function saleFactory(c) {
                    this.config = c;
                    this.psList = new Array();
                    // hard coded product specification
                    var item = new productSpecification_1.productSpecification();
                    item.id = '001A';
                    item.name = 'macbook air';
                    item.price = 1200;
                    this.psList.push(item);
                    item = new productSpecification_1.productSpecification();
                    item.id = '001B';
                    item.name = 'macbook';
                    item.price = 950;
                    this.psList.push(item);
                    item = new productSpecification_1.productSpecification();
                    item.id = '100';
                    item.name = '100 dollar bill!';
                    item.price = 100;
                    this.psList.push(item);
                }
                saleFactory.prototype.getProductSpecification = function (id) {
                    return this.psList.filter(function (item) {
                        return item.id === id;
                    })[0];
                };
                saleFactory.prototype.getVatCalculator = function () {
                    console.log(this.config);
                    if (typeof this.vatCalculator === 'undefined') {
                        if (this.config === 'MyVATCalculator')
                            this.vatCalculator = new MyVATCalculator_1.MyVATCalculator();
                        else
                            this.vatCalculator = new BDVATAdapter_1.BDVATAdapter();
                    }
                    return this.vatCalculator;
                };
                saleFactory.getInstance = function () {
                    if (typeof this.saleFactorySingleton === 'undefined') {
                        this.saleFactorySingleton = new saleFactory();
                    }
                    return this.saleFactorySingleton;
                };
                saleFactory.setConfig = function (c) {
                    x = this.getInstance();
                    x.config = c;
                };
                return saleFactory;
            })();
            exports_1("saleFactory", saleFactory);
        }
    }
});
//# sourceMappingURL=saleFactory.js.map