import {salesLineItem} from './salesLineItem';
import {saleFactory} from './saleFactory';



export class Sale{

	db: Array<SalesLineItem>;
	
	constructor(){
		this.db = new Array();
	}

	addSaleLineItem(id:string,q:number){
		sl = new salesLineItem(id,q);
		if (typeof sl.ps == 'undefined') alert('wrong product id!');
		else {
			this.db.push(sl);
			console.log("item added!", sl);
		}
		var x = this.getVatAmount();
		console.log("sale.ts: getVatAmount() ",x);
	}

	getTotal(){
		var sum = 0;
		for (var item of this.db) {
			sum+=item.getSubTotal()
		}
		return sum;
	}
	getVatAmount(){
		var ivac=saleFactory.getInstance().getVatCalculator();
		return ivac.getVATAmount(this.getTotal());
	}
	getGrandTotal(){
		return this.getTotal() + this.getVatAmount();
	}
}