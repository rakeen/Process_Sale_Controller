import {productSpecification} from './productSpecification';
import {saleFactory} from './saleFactory';

import {Http} from 'angular2/http';


export class salesLineItem {
	ps: productSpecification;
	quantity: number;

	constructor(id:string, q:number) {
		sf = saleFactory.getInstance();
		this.ps = sf.getProductSpecification(id);
		this.quantity = q;
	}

	getSubTotal(){
		return this.quantity * this.ps.getPrice();
	}
}