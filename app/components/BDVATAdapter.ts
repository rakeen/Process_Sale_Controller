import { IVATCalculator } from './ivatcalculator';
import { BDVATCalculator } from './BD/BDVATCalculator';

export class BDVATAdapter implements IVATCalculator {
	bdv: BDVATCalculator;
	constructor() { this.bdv = new BDVATCalculator(); }

	getVATAmount(saleTotal: number) {
		return parseInt(this.bdv.calculateVATAmount(saleTotal));		
	}
}