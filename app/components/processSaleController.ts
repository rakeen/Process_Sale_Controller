import { Component} from 'angular2/core';
import {Http, HTTP_BINDINGS} from 'angular2/http';
import 'rxjs/add/operator/map';


import {saleFactory} from './saleFactory';
import {Sale} from './sale';

@Component({
	selector: 'app',
	templateUrl: './app/components/processSaleController.html',
	styleUrls: [],
	providers: [Sale,HTTP_BINDINGS]
})






export class App {
	sale: Sale;


	// dummy variables to get the data
	itemId: string;
	quantity: number;
	res;

	constructor(sale:Sale,http:Http) {

		// setting the config
		http.get('/app/config.json').map(response => response.json())
									.subscribe(
										data=>	this.res = data,
										err =>	console.error('There was an error while fetching the config.json file: ' + err),
										()	=>	{ 
													saleFactory.setConfig(this.res.config);
													console.log('successfully fetched config.json!',this.res.config);
												}
									);


		this.sale = sale;
	}

	makeNewSale(){
		this.sale = new Sale();
		console.log('new sale!');
	}

	addItem() {
		this.sale.addSaleLineItem(this.itemId, this.quantity);

		// make the input fields empty again
		this.itemId = null;
		this.quantity = null;
	}

	getSale(){
		return this.sale;
	}
}
