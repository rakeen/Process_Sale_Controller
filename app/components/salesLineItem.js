System.register(['./saleFactory'], function(exports_1) {
    var saleFactory_1;
    var salesLineItem;
    return {
        setters:[
            function (saleFactory_1_1) {
                saleFactory_1 = saleFactory_1_1;
            }],
        execute: function() {
            salesLineItem = (function () {
                function salesLineItem(id, q) {
                    sf = saleFactory_1.saleFactory.getInstance();
                    this.ps = sf.getProductSpecification(id);
                    this.quantity = q;
                }
                salesLineItem.prototype.getSubTotal = function () {
                    return this.quantity * this.ps.getPrice();
                };
                return salesLineItem;
            })();
            exports_1("salesLineItem", salesLineItem);
        }
    }
});
//# sourceMappingURL=salesLineItem.js.map