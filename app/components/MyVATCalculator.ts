import { IVATCalculator } from './ivatcalculator';

export class MyVATCalculator implements IVATCalculator{
	constructor(){}

	getVATAmount(saleTotal: number) { 
		return Math.round(saleTotal * 0.05);
	}
}