System.register([], function(exports_1) {
    var MyVATCalculator;
    return {
        setters:[],
        execute: function() {
            MyVATCalculator = (function () {
                function MyVATCalculator() {
                }
                MyVATCalculator.prototype.getVATAmount = function (saleTotal) {
                    return Math.round(saleTotal * 0.05);
                };
                return MyVATCalculator;
            })();
            exports_1("MyVATCalculator", MyVATCalculator);
        }
    }
});
//# sourceMappingURL=MyVATCalculator.js.map