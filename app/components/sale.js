System.register(['./salesLineItem', './saleFactory'], function(exports_1) {
    var salesLineItem_1, saleFactory_1;
    var Sale;
    return {
        setters:[
            function (salesLineItem_1_1) {
                salesLineItem_1 = salesLineItem_1_1;
            },
            function (saleFactory_1_1) {
                saleFactory_1 = saleFactory_1_1;
            }],
        execute: function() {
            Sale = (function () {
                function Sale() {
                    this.db = new Array();
                }
                Sale.prototype.addSaleLineItem = function (id, q) {
                    sl = new salesLineItem_1.salesLineItem(id, q);
                    if (typeof sl.ps == 'undefined')
                        alert('wrong product id!');
                    else {
                        this.db.push(sl);
                        console.log("item added!", sl);
                    }
                    var x = this.getVatAmount();
                    console.log("sale.ts: getVatAmount() ", x);
                };
                Sale.prototype.getTotal = function () {
                    var sum = 0;
                    for (var _i = 0, _a = this.db; _i < _a.length; _i++) {
                        var item = _a[_i];
                        sum += item.getSubTotal();
                    }
                    return sum;
                };
                Sale.prototype.getVatAmount = function () {
                    var ivac = saleFactory_1.saleFactory.getInstance().getVatCalculator();
                    return ivac.getVATAmount(this.getTotal());
                };
                Sale.prototype.getGrandTotal = function () {
                    return this.getTotal() + this.getVatAmount();
                };
                return Sale;
            })();
            exports_1("Sale", Sale);
        }
    }
});
//# sourceMappingURL=sale.js.map